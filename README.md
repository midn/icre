## [Download](https://gitlab.com/midn/icre/-/raw/master/ICRE.zip)

# ICRE

Raymarching engine for the 8086. Minimum (should be) MS-DOS 2.0.

This program should not be used as an example of fast raymarching. It is very intentionally slow, for the customizability tradeoff.

The code is also not meant to be readable, at least yet.

# How to Use

The suite contains the raymarcher, `ICRE`, and the scene compiler, `ICREC`. To try a scene, first compile it:

    ICREC TOWER.ICR TOWER.OUT

Run `ICRE` on the output file:

    ICRE TOWER.OUT

The scene definition language is fully used in the examples.

# Building

Building requires a 386+ processor.

To build the raymarcher, include the NASM for DOS installation in the `nasm` subdirectory, and the VAL linker in the `val` subdirectory. Afterwards, run `BUILD.BAT` from within MS-DOS.
To build the compiler, install the OpenWatcom toolchain, and run `wmake` on the makefile.

# To-do List

* Add more complex SDFs.

# Screenshots

![Grid of cube pairs](screenshots/0.png)
