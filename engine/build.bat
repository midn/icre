@echo off

mkdir obj
..\nasm\nasm -fobj -o obj\icre.o icre.asm
..\nasm\nasm -fobj -o obj\vec3.o vec3.asm
..\nasm\nasm -fobj -o obj\sdfs.o sdfs.asm
..\nasm\nasm -fobj -o obj\math.o math.asm

..\val\val obj\icre.o obj\vec3.o obj\sdfs.o obj\math.o, ..\icre.exe, , ,