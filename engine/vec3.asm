cpu 8086

segment CODE

; In-stack:
;	f3 vec;
; Out-stack:
;	f1 ret;
global Vec3_LengthSquared
Vec3_LengthSquared:
	fmul st0, st0
	fwait
	fxch st1
	fwait
	fmul st0, st0
	fwait
	faddp st1, st0
	fwait
	fxch st1
	fwait
	fmul st0, st0
	fwait
	faddp st1, st0
	fwait
	ret

; In-stack:
;	f3 vec;
; Out-stack:
;	f1 ret;
global Vec3_Length
Vec3_Length:
	call Vec3_LengthSquared
	fsqrt
	fwait
	ret

; In-Stack:
;	f3 vec;
; Out-stack:
;	f3 vec;
global Vec3_Abs
Vec3_Abs:
	fabs
	fxch st1
	fabs
	fxch st1
	fxch st2
	fabs
	fxch st2
	fabs
	ret

; In-stack:
;	f3 vec1;
;	f3 vec2;
; Out-stack:
;	f3 ret;
global Vec3_Add
Vec3_Add:
	faddp st3, st0
	fwait
	faddp st3, st0
	fwait
	faddp st3, st0
	fwait
	ret

; In-stack:
;	f3 what;
;	f3 from;
; Out-stack:
;	f3 ret;
global Vec3_Sub
Vec3_Sub:
	fsubp st3, st0
	fwait
	fsubp st3, st0
	fwait
	fsubp st3, st0
	fwait
	ret

; In-stack:
;	f3 vec;
;	f3;
; Out-stack:
;	f3 ret;
global Vec3_Normalize
Vec3_Normalize:
	fld st2
	fwait
	fld st2
	fwait
	fld st2
	fwait
	call Vec3_Length
	fdiv st1, st0
	fwait
	fdiv st2, st0
	fwait
	fdivp st3, st0
	fwait
	ret

; In-stack:
;	f1 scale;
;	f3 vec;
; Out-stack:
;	f3 vec;
global Vec3_Scale
Vec3_Scale:
	fmul st1, st0
	fwait
	fmul st2, st0
	fwait
	fmulp st3, st0
	fwait
	ret

; In:
;	ds:si = a
;	ds:di = b
; Out-stack:
;	f3 c;
global Vec3_Cross
Vec3_Cross:
	fld dword [ds:si + 0]
	fwait
	fld dword [ds:di + 4]
	fwait
	fmulp
	fwait
	fld dword [ds:si + 4]
	fwait
	fld dword [ds:di + 0]
	fwait
	fmulp
	fwait
	fsubp
	fwait
	
	fld dword [ds:si + 8]
	fwait
	fld dword [ds:di + 0]
	fwait
	fmulp
	fwait
	fld dword [ds:si + 0]
	fwait
	fld dword [ds:di + 8]
	fwait
	fmulp
	fwait
	fsubp
	fwait
	
	fld dword [ds:si + 4]
	fwait
	fld dword [ds:di + 8]
	fwait
	fmulp
	fwait
	fld dword [ds:si + 8]
	fwait
	fld dword [ds:di + 4]
	fwait
	fmulp
	fwait
	fsubp
	fwait
	
	ret