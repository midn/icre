cpu 8086

segment DATA

extern fpuTemp
extern EPSILON

segment CODE

; In-stack:
;	f1 a;
;	f1 b;
; Out-stack:
;	f1 ret;
global Math_Max
Math_Max:
	fcom st1
	fstsw [fpuTemp]
	test byte [fpuTemp + 1], 5
	jp .isA
	fstp st0
	fwait
	ret
.isA:
	fstp st1
	fwait
	ret

; In-stack:
;	f1 a;
;	f1 b;
; Out-stack:
;	f1 ret;
global Math_Min
Math_Min:
	fcom st1
	fstsw [fpuTemp]
	test byte [fpuTemp + 1], 5
	jp .isA
	fstp st1
	fwait
	ret
.isA:
	fstp st0
	fwait
	ret
