cpu 8086

segment DATA public

global ZERO_FLOAT, ONE_FLOAT, TWO_FLOAT, HALF_FLOAT

UP_VEC:
ZERO_FLOAT:
dd 0.0
ONE_FLOAT:
dd 1.0
dd 0.0
TWO_FLOAT: dd 2.0
HALF_FLOAT: dd 0.5

MAX_RAY_DEPTH: dd 12.0

camPos: resd 3
camForward: resd 3
camRight: resd 3
camUp: resd 3

STACK_SIZE equ 5
rayPosStack: resb 12 * STACK_SIZE
materialStack: resb 1 * STACK_SIZE
resultStack: resb 5 * STACK_SIZE ;A result is a 5-byte Mat+SgnDist pair

rayDir: resd 3
rayDepth: resd 1
rayTravelDistance: resd 1

uv: resd 2
uvIncrement: dd 0.02

codebuf: resb 4096
.size equ $ - codebuf

global EPSILON
EPSILON: dd 0.000005

global fpuTemp
fpuTemp: resw 1

bytecode:
	dw commandEnd
	dw commandSphere
	dw commandPushRes
	dw commandModulusStart
	dw commandModulusEnd
	dw commandMinRes
	dw commandCube
	dw commandPushMaterial
	dw commandPopMaterial
	dw commandMaxRes

segment STACK stack

resb 16384

segment CODE

extern SDFPrim_Sphere, SDFPrim_Cube
extern Vec3_Sub
extern Vec3_Normalize
extern Vec3_Scale
extern Vec3_Cross
extern Vec3_Add
extern SDFPrim_InfiniteRepetition

..start:
	finit
	
	mov bh, 0
	mov bl, [0x80] ; Cmd-line length.
	mov byte [0x82 + bx], 0 ;Replace terminator
	mov ax, 0x3D00
	mov dx, 0x82
	int 0x21
	jnc .foundFile
	mov ah, 0x4C
	int 0x21
.foundFile:
	mov bx, ax
	mov ax, DATA
	mov ds, ax
	mov es, ax
	mov dx, codebuf
	mov ah, 0x3F
	mov cx, codebuf.size
	int 0x21
	
	; Copy camera position and direction
	mov si, codebuf + 2
	mov di, camPos
	mov cx, 12
	rep movsw
	
	; Initialize 160x100 mode
	mov ax, 0x0003
	int 0x10
	mov dx, 0x3D8
	mov al, 1
	out dx, al
	mov dx, 0x3D4
	mov al, 4
	out dx, al
	inc dx
	mov al, 127
	out dx, al
	dec dx
	mov al, 6
	out dx, al
	inc dx
	mov al, 100
	out dx, al
	dec dx
	mov al, 7
	out dx, al
	inc dx
	mov al, 112
	out dx, al
	dec dx
	mov al, 9
	out dx, al
	inc dx
	mov al, 1
	out dx, al
	
	mov al, 9
	test word [codebuf], 1 ; Flags
	jz .noBlink
	or al, 32
.noBlink:
	mov dx, 0x3D8
	out dx, al
	
	mov al, [codebuf + 1]
	and al, 0xF
	mov [BACKGROUND_COLOR], al
	
.anim:
	call marchScreen
	
	mov ah, 0
	int 0x16
	
	cmp al, 27
	jne .anim
	
	mov ax, 0x0003
	int 0x10
	
	mov ah, 0x4C ;Exit.
	int 0x21

marchScreen:
	
	; Clear the screen and set the characters to 0xDE.
	mov ax, 0xB800
	mov es, ax
	xor di, di
	mov ax, 0x00DE
	mov cx, 8000
	rep stosw
	
	mov di, 1;We work with colors, so start at 1.
	
	mov word [uv + 4], __float32__(-1.0) & ((1 << 16) - 1)
	mov word [uv + 6], __float32__(-1.0) >> 16
	
.raymarchingLoopY:
	mov word [uv + 0], __float32__(-1.6) & ((1 << 16) - 1)
	mov word [uv + 2], __float32__(-1.6) >> 16
	
	mov cx, 80
	
	push cx
	push ax
	push bx
	push dx
	push si
	
.raymarchingLoopX:
	
	push di
	call shootRay
	pop di
	jc .char2
	shl al, 1
	shl al, 1
	shl al, 1
	shl al, 1
	or byte [es:di], al
.char2:
	
	call nextX
	
	push di
	call shootRay
	pop di
	jc .charDone
	or byte [es:di], al
.charDone:
	
	call nextX
	
	add di, 2
	
	loop .raymarchingLoopX
	
	pop si
	pop dx
	pop bx
	pop ax
	pop cx
	
	call nextY
	
	cmp di, 16000
	jb .raymarchingLoopY
	
	ret

; Returns whether hit or not in carry flag.
shootRay:
	; rayPos = camPos
	mov ax, [camPos + 0]
	mov [rayPosStack + 0], ax
	mov ax, [camPos + 2]
	mov [rayPosStack + 2], ax
	mov ax, [camPos + 4]
	mov [rayPosStack + 4], ax
	mov ax, [camPos + 6]
	mov [rayPosStack + 6], ax
	mov ax, [camPos + 8]
	mov [rayPosStack + 8], ax
	mov ax, [camPos + 10]
	mov [rayPosStack + 10], ax
	
	mov byte [materialStack], 0x04
	
	call calcRayDir
	
	;~ mov word [ds:rayDir + 0], __float32__(0.0) & ((1 << 16) - 1)
	;~ mov word [ds:rayDir + 2], __float32__(0.0) >> 16
	;~ mov word [ds:rayDir + 4], __float32__(0.0) & ((1 << 16) - 1)
	;~ mov word [ds:rayDir + 6], __float32__(0.0) >> 16
	;~ mov word [ds:rayDir + 8], __float32__(1.0) & ((1 << 16) - 1)
	;~ mov word [ds:rayDir + 10], __float32__(1.0) >> 16
	
	mov word [rayDepth + 0], __float32__(0.0) & ((1 << 16) - 1)
	mov word [rayDepth + 2], __float32__(0.0) >> 16
	
	mov dx, 800 ;iteration limit
.marchiter:
	dec dx
	jz .endIterLimit
	
	mov di, codebuf + 26
	mov si, rayPosStack
	mov bp, resultStack
	
.loopbytecode:
	mov bx, [di]
	call [bytecode + bx]
	jmp .loopbytecode
	
.cmpeps:
	fld dword [EPSILON]
	fcomp
	fstsw [fpuTemp]
	test byte [fpuTemp + 1], 65
	jz .end
	
	fld dword [rayDepth]
	fadd st0, st1
	fld dword [MAX_RAY_DEPTH]
	fcomp
	fstsw [fpuTemp]
	fstp dword [rayDepth]
	test byte [fpuTemp + 1], 1
	jnz .endNoHit
	
	fld dword [rayDir + 8]
	fld dword [rayDir + 4]
	fld dword [rayDir + 0]
	fxch st1
	fxch st2
	fxch st3
	call Vec3_Scale
	fld dword [si + 8]
	fld dword [si + 4]
	fld dword [si + 0]
	call Vec3_Add
	fstp dword [si + 0]
	fstp dword [si + 4]
	fstp dword [si + 8]
	
	jmp .marchiter
	
.end:
	fstp st0
	mov al, [bp - 5]
	ret
.endNoHit:
	fstp st0
BACKGROUND_COLOR equ $ + 1
	mov al, 0x08
	ret
.endIterLimit:
	mov al, 0x03
	ret

calcRayDir:
	; camRight = normalize(cross(UP_VEC, camForward))
	mov si, UP_VEC
	mov di, camForward
	call Vec3_Cross
	call Vec3_Normalize
	fstp dword [camRight + 0]
	fstp dword [camRight + 4]
	fstp dword [camRight + 8]
	
	; camUp = normalize(cross(camForward, camRight))
	mov si, camForward
	mov di, camRight
	call Vec3_Cross
	call Vec3_Normalize
	fstp dword [camUp + 0]
	fstp dword [camUp + 4]
	fstp dword [camUp + 8]
	
	; rayDir = normalize(uv.x * camRight + uv.y * camUp + camForward * 2)
	fld dword [camRight + 8]
	fld dword [camRight + 4]
	fld dword [camRight + 0]
	fld dword [uv + 0]
	call Vec3_Scale
	
	fld dword [camUp + 8]
	fld dword [camUp + 4]
	fld dword [camUp + 0]
	fld dword [uv + 4]
	call Vec3_Scale
	
	call Vec3_Add
	
	fld dword [camForward + 8]
	fld dword [camForward + 4]
	fld dword [camForward + 0]
	fld dword [TWO_FLOAT]
	call Vec3_Scale
	
	call Vec3_Add
	
	call Vec3_Normalize
	
	fstp dword [rayDir + 0]
	fstp dword [rayDir + 4]
	fstp dword [rayDir + 8]
	
	ret

nextX:
	fld dword [uv + 0]
	fld dword [uvIncrement]
	faddp
	fstp dword [uv + 0]
	ret

nextY:
	fld dword [uv + 4]
	fld dword [uvIncrement]
	faddp
	fstp dword [uv + 4]
	ret

commandEnd:
	fld dword [bp - 4]
	add sp, 2
	jmp shootRay.cmpeps

commandSphere:
	fld dword [si + 8]
	fld dword [di + 14]
	fsubp
	fmul st0, st0
	fld dword [si + 4]
	fld dword [di + 10]
	fsubp
	fmul st0, st0
	faddp
	fld dword [si + 0]
	fld dword [di + 6]
	fsubp
	fmul st0, st0
	faddp
	fsqrt
	fld dword [di + 2]
	fsubp
	add di, 18
	ret

commandPushRes:
	mov al, [materialStack]
	mov byte [bp], al
	fstp dword [bp + 1]
	add bp, 5
	add di, 2
	ret

commandModulusStart:
	fld dword [si + 8]
	fld dword [si + 4]
	fld dword [si + 0]
	call SDFPrim_InfiniteRepetition
	add si, 12
	fstp dword [si + 0]
	fstp dword [si + 4]
	fstp dword [si + 8]
	add di, 2
	ret

commandModulusEnd:
	sub si, 12
	add di, 2
	ret

commandMinRes:
	add di, 2
	sub bp, 5
	fld dword [bp + 1]
	fld dword [bp - 4]
	fcomp
	fstsw [fpuTemp]
	fstp st0
	test byte [fpuTemp + 1], 65
	jnz .z
	mov al, [bp + 0]
	mov [bp - 5], al
	mov ax, [bp + 1]
	mov [bp - 4], ax
	mov ax, [bp + 3]
	mov [bp - 2], ax
.z:
	ret

commandCube:
	fld dword [di + 10]
	fld dword [di + 6]
	fld dword [di + 2]
	
	fld dword [si + 8]
	fld dword [di + 22]
	fsubp
	fld dword [si + 4]
	fld dword [di + 18]
	fsubp
	fld dword [si + 0]
	fld dword [di + 14]
	fsubp
	call SDFPrim_Cube
	add di, 26
	ret

commandPushMaterial:
	mov ax, [materialStack + 2]
	mov [materialStack + 3], ax
	mov ax, [materialStack + 0]
	mov [materialStack + 1], ax
	mov al, [di + 2]
	mov [materialStack], al
	add di, 4
	ret

commandPopMaterial:
	mov ax, [materialStack + 1]
	mov [materialStack + 0], ax
	mov ax, [materialStack + 3]
	mov [materialStack + 2], ax
	add di, 2
	ret

commandMaxRes:
	add di, 2
	sub bp, 5
	fld dword [bp + 1]
	fld dword [bp - 4]
	fcomp
	fstsw [fpuTemp]
	fstp st0
	test byte [fpuTemp + 1], 65
	jz .z
	mov al, [bp + 0]
	mov [bp - 5], al
	mov ax, [bp + 1]
	mov [bp - 4], ax
	mov ax, [bp + 3]
	mov [bp - 2], ax
.z:
	ret