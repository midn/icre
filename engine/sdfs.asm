cpu 8086

segment DATA public

extern HALF_FLOAT
extern ONE_FLOAT
extern ZERO_FLOAT

segment CODE

extern Vec3_Length, Vec3_Abs, Math_Max, Math_Min

; In-stack:
;	f3 pos;
;	f1 radius;
; Out-stack:
;	f1 sd;
global SDFPrim_Sphere
SDFPrim_Sphere:
	call Vec3_Length
	fsubrp st1, st0
	fwait
	ret

; In-stack:
;	f3 pos;
;	f3 hsize;
; Out-stack:
;	f1 sd;
global SDFPrim_Cube
SDFPrim_Cube:
	call Vec3_Abs
	fsub st0, st3
	fstp st3
	fsub st0, st3
	fstp st3
	fsub st0, st3
	fstp st3
	
	fld st2 ; z
	fld st2 ; y
	call Math_Max
	fld st1 ; x
	call Math_Max
	fld dword [ZERO_FLOAT]
	call Math_Min
	; Move min(max(q.x, max(q.y, q.z)), 0.0) to the bottom of the stack
	fxch st3
	fxch st2
	fxch st1
	
	fld dword [ZERO_FLOAT]
	call Math_Max
	fxch st1
	fld dword [ZERO_FLOAT]
	call Math_Max
	fxch st1
	fxch st2
	fld dword [ZERO_FLOAT]
	call Math_Max
	fxch st2
	call Vec3_Length
	
	faddp
	
	ret

global SDFPrim_InfiniteRepetition
SDFPrim_InfiniteRepetition:
	fld dword [HALF_FLOAT]
	fadd st1, st0
	fadd st2, st0
	faddp st3, st0
	fld dword [ONE_FLOAT]
	fxch st1
	fprem
	fxch st2
	fprem
	fxch st2
	fxch st3
	fprem
	fxch st3
	fxch st1
	fstp st0
	fld dword [HALF_FLOAT]
	fsub st1, st0
	fsub st2, st0
	fsubp st3, st0
	ret