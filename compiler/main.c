#include<stdio.h>
#include<stdint.h>
#include<errno.h>
#include<math.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<stddef.h>

#ifdef __WATCOMC__
float sqrtf(float);
#pragma aux sqrtf = "fsqrt" parm [8087] value [8087];
#else
#include<math.h>
#endif

typedef float Vec3[3];

#define TYPE_STR 0
#define TYPE_VEC3 1
#define TYPE_NUM 2
#define TYPE_BLOCK 3
typedef struct {
	uint8_t type;
	union {
		char *str;
		Vec3 vec3;
		fpos_t blockPos;
	};
} Value;

FILE *in, *out;

void lex_unget() {
	fseek(in, -1, SEEK_CUR);
}

int lex_char() {
	int c;
	while(c = fgetc(in)) {
		if(c == -1) return -1;
		else if(!isspace(c)) break;
	}
	
	return c;
}

void lex_expect(int c) {
	int got = lex_char();
	if(got != c) {
		printf("Expected %c, got %c\n", c, got);
		abort();
	}
}

int lex_peek() {
	int r = lex_char();
	lex_unget();
	return r;
}

char *lex_string_literal() {
	{
		int c = lex_char();
		if(c == -1) return NULL;
		else if(c != '\"') {
			lex_unget();
			return NULL;
		}
	}
	
	size_t capacity = 8, index = 0;
	char *buf = malloc(capacity);
	
	for(;;) {
		if(index == capacity) {
			buf = realloc(buf, capacity += 8);
		}
		
		buf[index] = fgetc(in);
		
		if(buf[index] == '\"') {
			buf[index] = 0;
			break;
		}
		
		index++;
	}
	
	return buf;
}

float lex_num() {
	size_t capacity = 8, index = 0;
	char *buf = malloc(capacity);
	
	{
		int c = lex_char();
		if(c == '-') {
			buf[index++] = '-';
		} else ungetc(c, in);
	}
	
	for(;;) {
		if(index == capacity) {
			buf = realloc(buf, capacity += 8);
		}
		
		buf[index] = fgetc(in);
		
		if(!isdigit(buf[index]) && buf[index] != '.') {
			ungetc(buf[index], in);
			buf[index] = 0;
			break;
		}
		
		index++;
	}
	
	float ret = atof(buf);
	
	free(buf);
	
	return ret;
}

int parse_vec3(Vec3 ret) {
	memset(ret, 0, sizeof(Vec3));
	
	if(lex_char() != '(') {
		ret[0] = lex_num();
		return 1;
	}
	
	for(int i = 0; i < 3; i++) {
		ret[i] = lex_num();
		
		int c = lex_char();
		if(c == ')') break;
		else if(c != ',') abort();
	}
	
	return 1;
}

void parse_skip_val() {
	int c = lex_char();
	
	if(isdigit(c)) lex_num();
	else if(c == '\"') while(lex_char() != '\"');
	else if(c == '(') while(lex_char() != ')');
	else if(c == '{') {
		int depth = 1;
		
		while(depth > 0) {
			c = lex_char();
			if(c == '\"') while(lex_char() != '\"');
			else if(c == '{') depth++;
			else if(c == '}') depth--;
		}
	}
}

int parse_obj() {
	while(1) {
		char *type = lex_string_literal();
		
		if(feof(in)) return 0;
		else if(!type) parse_skip_val();
		
		if(!strcmp(type, "Sphere")) {
			const uint16_t id0 = 2, id1 = 4;
			fwrite(&id0, 2, 1, out);
			
			Vec3 v;
			parse_vec3(v); /* rad */
			fwrite(v, sizeof(float), 1, out);
			
			parse_vec3(v); /* pos */
			fwrite(v, sizeof(Vec3), 1, out);
			
			fwrite(&id1, 2, 1, out);
			
			break;
		} else if(!strcmp(type, "Union")) {
			const uint16_t id = 10;
			
			lex_expect('{');
			
			parse_obj();
			
			parse_obj();
			
			fwrite(&id, 2, 1, out);
			
			lex_expect('}');
			
			break;
		} else if(!strcmp(type, "Modulus")) {
			const uint16_t id0 = 6, id1 = 8;
			fwrite(&id0, 2, 1, out);
			
			lex_expect('{');
			
			parse_obj();
			
			fwrite(&id1, 2, 1, out);
			
			lex_expect('}');
			
			break;
		} else if(!strcmp(type, "Cube")) {
			const uint16_t id0 = 12, id1 = 4;
			fwrite(&id0, 2, 1, out);
			
			Vec3 v;
			parse_vec3(v); /* hsize */
			fwrite(v, sizeof(Vec3), 1, out);
			
			parse_vec3(v); /* pos */
			fwrite(v, sizeof(Vec3), 1, out);
			
			fwrite(&id1, 2, 1, out);
			
			break;
		} else if(!strcmp(type, "Material")) {
			Vec3 v;
			parse_vec3(v);

			const uint16_t id0 = 14, id1 = 16;
			fwrite(&id0, 2, 1, out);
			
			const uint16_t m = (uint8_t) v[0];
			fwrite(&m, 2, 1, out);
			
			lex_expect('{');
			parse_obj();
			lex_expect('}');
			
			fwrite(&id1, 2, 1, out);
			
			break;
		} else if(!strcmp(type, "Intersection")) {
			const uint16_t id = 18;
			
			lex_expect('{');
			
			parse_obj();
			
			parse_obj();
			
			fwrite(&id, 2, 1, out);
			
			lex_expect('}');
			
			break;
		} else parse_skip_val();
	}
	
	return 1;
}

void parse_root() {
	char *k;
	
	{
		Vec3 camPos = {0, 0, 0};
		
		int dirIsTarget = 0;
		Vec3 camDir = {0, 0, -1};
		
		uint16_t flags = 0;
		
		while(1) {
			k = lex_string_literal();
			
			if(!k) {
				if(feof(in)) {
					break;
				} else {
					parse_skip_val();
					continue;
				}
			}
			
			if(!strcmp(k, "Camera Position")) {
				parse_vec3(camPos);
			} else if(!strcmp(k, "Camera Direction")) {
				dirIsTarget = 0;
				parse_vec3(camDir);
			} else if(!strcmp(k, "Camera Target")) {
				dirIsTarget = 1;
				parse_vec3(camDir);
			} else if(!strcmp(k, "CGA Blink")) {
				Vec3 temp;
				parse_vec3(temp);
				flags |= temp[0] != 0;
			} else if(!strcmp(k, "Background")) {
				Vec3 temp;
				parse_vec3(temp);
				flags |= (((uint16_t) temp[0]) & 0xF) << 8;
			}
		}
		
		if(dirIsTarget) {
			camDir[0] -= camPos[0];
			camDir[1] -= camPos[1];
			camDir[2] -= camPos[2];
		}
		
		float s = sqrtf(camDir[0] * camDir[0] + camDir[1] * camDir[1] + camDir[2] * camDir[2]);
		camDir[0] /= s;
		camDir[1] /= s;
		camDir[2] /= s;
		
		fwrite((void*) &flags, sizeof(flags), 1, out);
		fwrite((void*) camPos, sizeof(Vec3), 1, out);
		fwrite((void*) camDir, sizeof(Vec3), 1, out);
	}
	
	fseek(in, 0, SEEK_SET);
	
	parse_obj();
	
	const uint16_t id = 0;
	fwrite(&id, 2, 1, out);
}

int main(int argc, char **argv) {
	if(argc != 3) {
		puts("Usage: ICREC <input FN> <output FN>");
		return 1;
	}
	
	in = fopen(argv[1], "rb");
	if(!in) {
		printf("Error: %s", strerror(errno));
		return 2;
	}
	
	out = fopen(argv[2], "wb");
	if(!out) {
		printf("Error: %s", strerror(errno));
		return 3;
	}
	
	parse_root();
	
	fclose(in);
	fclose(out);
	
	return 0;
}